const jwt = require("jsonwebtoken");
const User = require("../../models/user-model");
const Course = require("../../models/course-model");
const formatResponse = require("../../utils/format-response");

// Check if the user is authenticated. Authenticated means the user has logged in successfully with a valid token.
const isAuthenticatedUser = async (req, res, next) => {
  let token;

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    try {
      token = req.headers.authorization.split(" ")[1];

      const decoded = jwt.verify(token, process.env.JWT_SECRET);

      // Find the user by id and exclude the password field.
      req.user = await User.findById(decoded._id).select("-password");

      // If the user is not found, return error message.
      if (!req.user) {
        return res.status(401).json(formatResponse(req, false, "User not found.", {}, [{
          error_code: "USER_NOT_FOUND", error_message: "User not found!",
        }]));
      }

      next();
    } catch (error) {
      console.log(error);
      return res.status(401).json(formatResponse(req, false, "Not authorized, token failed.", {}, [{
        error_code: "TOKEN_FAILED", error_message: error.message,
      }]));
    }
  }

  if (!token) {
    return res.status(401).json(formatResponse(req, false, "Not authorized, no token available. Please login first.", {}, [{
      error_code: "NO_TOKEN", error_message: "Not authorized, no token available. Please login first.",
    }]));
  }
};

// Check if the user is an admin.
const isAdmin = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next();
  } else {
    return res.status(401).json(formatResponse(req, false, "Not authorized as an admin.", {}, [{
      error_code: "NOT_AUTHORIZED_AS_ADMIN", error_message: "Not authorized as an admin.",
    }]));
  }
};

// Check if the user is an instructor.
const isInstructor = (req, res, next) => {
  if (req.user && req.user.isInstructor()) {
    next();
  } else {
    return res.status(401).json(formatResponse(req, false, "Only instructor can post a new course.", {}, [{
      error_code: "NOT_AUTHORIZED_AS_INSTRUCTOR", error_message: "Only instructor can post a new course.",
    }]));
  }
};

// Check if the user is a course member, including the instructor or one of the students of the course.
const isCourseMember = async (req, res, next) => {
  const { _id } = req.params;

  Course.findById(_id)
    .then((course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      if (
        course.instructor.toString() === req.user._id.toString() ||
        course.teaching_assistants.includes(req.user._id) ||
        course.students.includes(req.user._id)
      ) {
        next();
      } else {
        return res.status(401).json(formatResponse(req, false, "Only course member can access this course.", {}, [{
          error_code: "NOT_AUTHORIZED_AS_COURSE_MEMBER", error_message: "Only course member can access this course.",
        }]));
      }
    })
    .catch((err) => {
      res.status(400).json(formatResponse(req, false, "Get course failed", {}, [{
        error_code: "GET_COURSE_FAILED", error_message: err.message,
      }]));
    });
};

module.exports = { isAuthenticatedUser, isAdmin, isInstructor, isCourseMember };
