const express = require("express");
const router = express.Router();

const userRoutes = require("./user-route");
const courseRoutes = require("./course-route");
const authRoutes = require("./auth-route");
const paymentRoutes = require("./payment-route");
const transactionRoutes = require("./transaction-route");
const pointRoutes = require("./point-route");
const challengeRoutes = require("./challenge-route");
const cartRoutes = require("./cart-route");


router.use("/user", userRoutes);
router.use("/course", courseRoutes);
router.use("/auth", authRoutes);
router.use("/payment", paymentRoutes);
router.use("/transaction", transactionRoutes);
router.use("/point", pointRoutes);
router.use("/challenge", challengeRoutes);
router.use("/cart", cartRoutes);


module.exports = router;
