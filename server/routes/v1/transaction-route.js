const router = require("express").Router();
const {
  addPurchaseRecord,
  addRefundRecord,
} = require("../../controllers/v1/transaction-controller");

const { isAuthenticatedUser } = require("../../middlerware/v1/auth-middleware");

router.route("/purchase").put(isAuthenticatedUser, addPurchaseRecord);
router.route("/refund").put(isAuthenticatedUser, addRefundRecord);

module.exports = router;
