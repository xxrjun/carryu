const express = require("express");
const router = express.Router();
const { processPayment } = require("../../controllers/v1/payment-controller");
const { isAuthenticatedUser } = require("../../middlerware/v1/auth-middleware");

router.route("/").get(isAuthenticatedUser, processPayment);

module.exports = router;
