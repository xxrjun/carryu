const express = require("express");
const router = express.Router();
const { processPayment } = require("../../controllers/v2/payment-controller");
const { isAuthenticatedUser } = require("../../middlerware/v2/auth-middleware");

router.route("/").get(isAuthenticatedUser, processPayment);

module.exports = router;
