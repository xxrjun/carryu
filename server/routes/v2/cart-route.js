const express = require("express");
const router = express.Router();
const { isAuthenticatedUser } = require("../../middlerware/v2/auth-middleware");
const {
  addCourseToCart,
  getCartItems,
  removeCourseFromCart,
} = require("../../controllers/v2/cart-controller");

router
  .route("/")
  .get(isAuthenticatedUser, getCartItems)
  .post(isAuthenticatedUser, addCourseToCart)
  .delete(isAuthenticatedUser, removeCourseFromCart);

module.exports = router;
