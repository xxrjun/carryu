const express = require("express");
const router = express.Router();

// Import v1 and v2 routes
const v1Routes = require("./v1");
const v2Routes = require("./v2");

// Use v1 and v2 routes
router.use("", v1Routes);
router.use("/v2", v2Routes);

module.exports = router;
