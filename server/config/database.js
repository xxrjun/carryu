const mongoose = require("mongoose");

mongoose.set('strictQuery', true);

const connectDB = () => {
  console.log("Connecting to database...");

  return mongoose
    .connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() =>
      console.log(`MongoDB connected! Connected to database ${mongoose.connection.db.databaseName}`),
    )
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

module.exports = connectDB;
