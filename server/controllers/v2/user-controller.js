const User = require("../../models/user-model.js");
const registerValidation =
  require("../../validations/validation.js").registerValidation;
const updateProfileValidation =
  require("../../validations/validation.js").updateProfileValidation;
const passwordValidation =
  require("../../validations/validation.js").passwordValidation;
const formatResponse = require("../../utils/format-response.js");

// @desc    Register new user
// @route   POST api/user/register
// @access  Public
exports.registerUser = async (req, res) => {
  // Destruct register data from request body.
  let { username, email, password } = req.body;

  // Check if register data is valid.
  const { error } = registerValidation({ username, email, password });
  if (error) return res.status(400).json(formatResponse(req, false, "Validation failed.", {}, [{
    error_code: "VALIDATION_ERROR", error_message: error.details[0].message,
  }]));

  // Check if the email already exists.
  const emailExist = await User.findOne({ email: email });

  // If email exists, return error message.
  if (emailExist) {
    return res.status(400).json(formatResponse(req, false, "Email has already been registered.", {}, [{
      error_code: "EMAIL_ALREADY_REGISTERED", error_message: "Email has already been registered.",
    }]));
  }

  // Create new User object
  const newUser = new User({
    username,
    email,
    password,
  });

  try {
    // Save user into database
    const savedUser = await newUser.save();
    return res.status(200).json(formatResponse(req, true, "Register successfully", savedUser));
  } catch (err) {
    return res.status(400).json(formatResponse(req, false, "Register failed.", {}, [{
      error_code: "REGISTER_FAILED", error_message: err.message,
    }]));
  }
};

// @desc    Get user's profile
// @route   GET api/user/profile
// @access  Private
exports.getUserProfile = async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(404).json(formatResponse(req, false, "User not found.", {}, [{
      error_code: "USER_NOT_FOUND", error_message: "User not found!",
    }]));
  }

  return res.status(200).json(formatResponse(req, true, "Get user profile successfully", {
    _id: user._id,
    username: user.username,
    email: user.email,
    role: user.role,
    register_time: user.register_time,
  }));
};

// @desc    Update user's profile
// @route   PATCH api/user/profile
// @access  Private
exports.updateUserProfile = async (req, res) => {
  // Check if the input email already exists.
  const emailExist = await User.findOne({
    email: req.body.email,
  });


  // Destruct user from request.
  const { user } = req;

  username = req.body.username || user.username;
  email = req.body.email || user.email;

  // Check if the updated data is valid.
  const { error } = updateProfileValidation({ username, email });
  if (error) return res.status(400).json(formatResponse(req, false, "Validation failed.", {}, [{
    error_code: "VALIDATION_ERROR", error_message: error.details[0].message,
  }]));

  // If the updated data is valid, update the user's profile and save to database.
  user.username = username;
  user.email = email;

  const updatedUser = await user.save();

  return res.status(200).json(formatResponse(req, true, "Update user profile successfully", {
    _id: updatedUser._id,
    username: updatedUser.username,
    email: updatedUser.email,
    role: updatedUser.role,
    register_time: updatedUser.register_time,
  }));
};

// @desc    Get my purchase history
// @route   GET api/user/purchase-history
// @access  Private
exports.getUserPurchaseHistory = async (req, res) => {
  // get user purchase history from database, sorted by date
  User.findById(req.user._id)
    .populate("purchase_history")
    .exec(function(err, user) {
      if (err) {
        return res.status(400).json(formatResponse(req, false, "Get purchase history failed.", {}, [{
          error_code: "GET_PURCHASE_HISTORY_FAILED", error_message: err.details[0].message,
        }]));

      }

      const purchase_history = user.purchase_history.sort(
        (a, b) => b.date - a.date,
      );

      return res.status(200).json(formatResponse(req, true, "Get purchase history successfully",
        purchase_history,
      ));
    });
};

// @desc    Update user's password
// @route   PATCH api/user/password
// @access  Private
exports.updateUserPassword = async (req, res) => {
  console.log("Update user's password");

  // Get user by id and select the password field
  // Use `+` to override schema-level `select: false` without making the projection inclusive
  const user = await User.findById(req.user._id).select("+password");

  if (!user) {
    return res.status(404).json(formatResponse(req, false, "User not found.", {}, [{
      error_code: "USER_NOT_FOUND", error_message: "User not found!",
    }]));
  }

  // Deconstruct the request body
  const { old_password, new_password, confirm_password } = req.body;

  // Three password fields are required.
  if (!(old_password && new_password && confirm_password)) {
    return res.status(400).json(formatResponse(req, false, "All password fields are required.", {}, [{
      error_code: "PASSWORD_FIELDS_REQUIRED", error_message: "All password fields are required.",
    }]));
  }

  // Compare the input old_password is correct.
  user.comparePassword(old_password, async function(err, isMatch) {
    if (err) {
      return res.status(500).json(formatResponse(req, false, "Cannot compare password.", {}, [{
        error_code: "COMPARE_PASSWORD_FAILED", error_message: err.message,
      }]));
    }

    if (isMatch) {
      // Check if the new password is valid.
      const { error } = passwordValidation({ password: new_password });
      if (error)
        return res.status(400).json(formatResponse(req, false, "Validation failed.", {}, [{

          error_code: "VALIDATION_ERROR", error_message: error.details[0].message,
        }]));

      // Check if the new password is equal to confirmation one.
      if (new_password !== confirm_password) {
        return res.status(400).json(formatResponse(req, false, "New password and confirmation password do not match.", {}, [{
          error_code: "PASSWORD_NOT_MATCH", error_message: "New password and confirmation password do not match.",
        }]));
      }

      // Update the user's password and save to database if everything is fine:).
      user.password = new_password;
      await user.save();

      return res.status(200).json(formatResponse(req, true, "Password updated successfully"));
    } else {
      return res.status(401).json(formatResponse(req, false, "Old password is incorrect.", {}, [{
        error_code: "OLD_PASSWORD_INCORRECT", error_message: "Old password is incorrect.",
      }]));
    }
  });
};

// @desc    Switch a user role to instructor
// @route   PATCH api/user/switch-role
// @access  Private
exports.switchUserRole = async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(404).json(formatResponse(req, false, "User not found.", {}, [{
      error_code: "USER_NOT_FOUND", error_message: "User not found!",
    }]));
  }

  if (user.role === "student") {
    user.role = "instructor";
  }

  const updatedUser = await user.save();

  return res.status(200).json(formatResponse(req, true, "Switch user role successfully", {
    _id: updatedUser._id,
    username: updatedUser.username,
    email: updatedUser.email,
    role: updatedUser.role,
    register_time: updatedUser.register_time,
  }));
};

// @desc    Delete a user by id
// @route   DELETE api/user/delete-user/:id
// @access  Private/Admin
exports.deleteUser = async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user) {
    return res.status(404).json(formatResponse(req, false, "User not found.", {}, [{
      error_code: "USER_NOT_FOUND", error_message: "User not found!",
    }]));
  }

  const deletedUser = await User.findOneAndDelete({ email: req.body.email });

  return res.status(200).json(formatResponse(req, true, "Delete user successfully", {
    _id: deletedUser._id,
    username: deletedUser.username,
    email: deletedUser.email,
    role: deletedUser.role,
    register_time: deletedUser.register_time,
  }));
};

// @desc    Get single user by id
// @route   GET api/user/:id
// @access  Private/Admin
exports.getSingleUserById = async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user) {
    return res.status(404).json(formatResponse(req, false, "User not found.", {}, [{
      error_code: "USER_NOT_FOUND", error_message: "User not found!",
    }]));
  }

  return res.status(200).json(formatResponse(req, true, "Get user successfully", {
    _id: user._id,
    username: user.username,
    email: user.email,
    role: user.role,
    register_time: user.register_time,
  }));
};

// @desc    Get all user
// @route   GET api/user/all
// @access  Private/Admin
exports.getAllUser = async (req, res) => {
  const users = await User.find({});

  if (!users) {
    return res.status(404).json(formatResponse(req, false, "No user found.", {}, [{
      error_code: "USER_NOT_FOUND",
      error_message: "No user found!",
    }]));
  }

  return res.status(200).json(formatResponse(req, true, "Get all user successfully", {
    users,
  }));
};
