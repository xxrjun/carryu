const Course = require("../../models/course-model");
const User = require("../../models/user-model");
const courseValidation = require("../../validations/validation").courseValidation;
const formatResponse = require("../../utils/format-response");

// @desc    Create new course
// @route   POST api/course/create
// @access  Private/Instructor
exports.createNewCourse = (req, res) => {
  console.log("Creating new course...");
  console.log(req.body);

  // Destructuring the request body
  let { title, description, price, category } = req.body;
  const thumbnail = req.file ? req.file.filename : null;

  // Get the user from the request object
  const user = req.user;

  // Validate the course data.
  const { error } = courseValidation({ title, price, category });
  if (error) return res.status(400).json(formatResponse(req, false, "Validation failed.", {}, [{
    error_code: "VALIDATION_ERROR", error_message: error.details[0].message,
  }]));

  // Create new course object and save it into database.
  const course = new Course({
    title, description, price, thumbnail, category, instructor: user._id,
  });

  course
    .save()
    .then((result) => {
      return res.status(200).json(formatResponse(req, true, "Course created successfully",
        course,
      ));
    })
    .catch((err) => {
      console.log(err);
      return res.status(400).json(formatResponse(req, false, "Course creation failed.", {}, [{
        error_code: "COURSE_CREATION_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Update course info (title, description, price, thumbnail, category)
// @route   POST api/course/:_id/info
// @access  Private/Instructor
exports.updateCourseInfo = async (req, res) => {
  const { _id } = req.params;

  Course.findById(_id)
    .then((course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Check if the user is the instructor of the course.
      if (course.instructor.toString() !== req.user._id.toString()) {
        return res.status(403).json(formatResponse(req, false, "You are not the instructor of the course.", {}, [{
          error_code: "NOT_INSTRUCTOR", error_message: "You are not the instructor of the course.",
        }]));
      }

      const { title, description, price, category } = req.body;
      const thumbnail = req.file ? req.file.filename : null;

      // Update the course info.
      course.title = title || course.title;
      course.description = description || course.description;
      course.price = price || course.price;
      course.thumbnail = thumbnail || course.thumbnail;
      course.category = category || course.category;

      //Validate the course data.
      const { error } = courseValidation({
        title: course.title, price: course.price, category: course.category,
      });
      if (error) return res.status(400).json(formatResponse(req, false, "Validation failed.", {}, [{
        error_code: "VALIDATION_ERROR", error_message: error.details[0].message,
      }]));

      // Save the updated course info.
      course
        .save()
        .then((course) => {
          return res.status(200).json(formatResponse(req, true, "Course info updated successfully",
            course,
          ));
        })
        .catch((err) => {
          return res.status(400).json(formatResponse(req, false, "Course info update failed.", {}, [{
            error_code: "COURSE_INFO_UPDATE_FAILED", error_message: err.message,
          }]));
        });
    })
    .catch((err) => {
      res.status(400).json(formatResponse(req, false, "Course info update failed.", {}, [{
        error_code: "COURSE_INFO_UPDATE_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get course info by id
// @route   GET api/course/:_id/_info
// @access  Public
exports.getCourseInfoById = async (req, res) => {
  const { _id } = req.params;

  Course.findById(_id)
    .then(async (course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Get instructor name.
      const instructor = await User.findById(course.instructor);

      if (!instructor) {
        return res.status(404).json(formatResponse(req, false, "Instructor not found.", {}, [{
          error_code: "INSTRUCTOR_NOT_FOUND", error_message: "Instructor not found!",
        }]));
      }

      // Return the course info.
      return res.status(200).json(formatResponse(req, true, "Course found", {
        _id: course._id,
        title: course.title,
        instructor: instructor?.username || "Unknown Account",
        description: course.description,
        price: course.price,
        category: course.category,
        students_count: course.students.length,
        created: course.created,
      }));
    }).catch((err) => {
    return res.status(400).json(formatResponse(req, false, "Get course info failed.", {}, [{
      error_code: "GET_COURSE_INFO_FAILED", error_message: err.message,
    }]));
  });
};

// @desc    Get course content by id
// @route   GET api/course/:_id/content
// @access  Private/CourseMember
exports.getCourseContentById = async (req, res) => {
  console.log("Getting course content...");
  console.log(req.params);

  const { _id } = req.params;

  Course.findById(_id)
    .then(async (course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Get instructor name.
      const instructor = await User.findById(course.instructor);


      if (!instructor) {
        return res.status(404).json(formatResponse(req, false, "Instructor not found.", {}, [{
          error_code: "INSTRUCTOR_NOT_FOUND", error_message: "Instructor not found!",
        }]));
      }

      // Return the course content.
      return res.status(200).json(formatResponse(req, true, "Course found", {
        _id: course._id,
        title: course.title,
        instructor: instructor?.username || "Unknown Account",
        description: course.description,
        price: course.price,
        category: course.category,
        students_count: course.students.length,
        created: course.created,
        videos: course.videos,
        teaching_assistants: course.teaching_assistants,
      }));
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Get course content failed.", {}, [{
        error_code: "GET_COURSE_CONTENT_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get courses by title (fuzzy search)
// @route   GET api/course/search/:title
// @access  Public
exports.getCoursesByTitle = async (req, res) => {
  const { title } = req.params;
  // fuzzy search by title
  const courses = await Course.find({
    title: { $regex: title, $options: "i" }, $orderby: { views_count: -1 },
  }).select("-thumbnail -students -videos -teaching_assistants -comments");


  if (!courses) {
    return res.status(404).json(formatResponse(req, false, "No courses found.", {}, [{
      error_code: "COURSE_NOT_FOUND", error_message: "No courses found",
    }]));
  }

  return res.status(200).json(formatResponse(req, true, "Get courses by title successfully", {
    courses,
  }));
};

// @desc    Add course comment
// @route   POST api/course/:_id/comment
// @access  Private/CourseMember
exports.addCourseComment = async (req, res) => {
  console.log("Adding course comment...");

  const { _id } = req.params;

  console.log(req.body);

  Course.findById(_id)
    .then((course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Destructuring the request body
      let { comment } = req.body;

      if (!comment) {
        return res.status(400).json(formatResponse(req, false, "Comment cannot be empty.", {}, [{
          error_code: "COMMENT_CANNOT_BE_EMPTY", error_message: "Comment cannot be empty!",
        }]));
      }

      // Create a new comment object.
      const newComment = {
        user: req.user._id, comment, created: new Date(),
      };

      // Add the new comment to the course and save to database.
      course.comments.unshift(newComment);
      course
        .save()
        .then((course) => {
          return res.status(200).json(formatResponse(req, true, "Add course comment successfully",
            course.comments[0],
          ));
        })
        .catch((err) => {
          return res.status(400).json(formatResponse(req, false, "Add course comment failed.", {}, [{
            error_code: "ADD_COURSE_COMMENT_FAILED", error_message: err.message,
          }]));
        });
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Add course comment failed.", {}, [{
        error_code: "ADD_COURSE_COMMENT_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get course all comments
// @route   GET api/course/:_id/comment
// @access  Private/CourseMember
exports.getCourseAllComments = async (req, res) => {
  const { _id } = req.params;

  Course.findById(_id)
    .then((course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Return the course comments.
      return res.status(200).json(formatResponse(req, true, "Get course comments successfully", course.comments));
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Get course comments failed.", {}, [{
        error_code: "GET_COURSE_COMMENTS_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get all courses (descending order by views count)
// @route   Get api/course/all
// @access  Public
exports.getAllCourses = async (req, res) => {
  Course.find({})
    .select("-thumbnail -students -videos -teaching_assistants -comments")
    .then((courses) => {
      return res.status(200).json(formatResponse(req, true, "Get all courses successfully",
        courses,
      ));
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Get all courses failed.", {}, [{
        error_code: "GET_ALL_COURSES_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get ten most popular courses (descending order by students count)
// @route   GET api/course/popular
// @access  Public
exports.getTenMostPopularCourses = async (req, res) => {
  Course.find({})
    .sort({ students_count: -1 })
    .limit(10)
    .select("-thumbnail -students -videos -teaching_assistants -comments")
    .then((courses) => {
      return res.status(200).json(formatResponse(req, true, "Get ten most popular courses successfully", courses));
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Get ten most popular courses failed.", {}, [{
        error_code: "GET_TEN_MOST_POPULAR_COURSES_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get most ten popular newest
// @route   GET api/course/newest
// @access  Public
exports.getTenNewestCourses = async (req, res) => {
  Course.find({})
    .sort({ created: -1 })
    .limit(10)
    .select("-thumbnail -students -videos -teaching_assistants -comments")
    .then((courses) => {
      return res.status(200).json(formatResponse(req, true, "Get ten newest courses successfully", courses));
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Get ten newest courses failed.", {}, [{
        error_code: "GET_TEN_NEWEST_COURSES_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get courses by category (sort by views count)
// @route   GET api/course/category/:category
// @access  Public
exports.getCoursesByCategory = async (req, res) => {
  const { category } = req.params;

  const allCategories = ["web-development", "programming-language", "finance", "it-and-software", "art-design", "music", "system-design", "music", "marketing", "others"];

  if (!allCategories.includes(category)) {
    return res.status(400).json(formatResponse(req, false, "Category not found.", {}, [{
      error_code: "CATEGORY_NOT_FOUND", error_message: "Category not found!",
    }]));
  }

  Course.find({ category })
    .sort({ views_count: -1 })
    .select("-thumbnail -students -videos -teaching_assistants -comments")
    .then((courses) => {
      return res.status(200).json(formatResponse(req, true, `Get courses by category ${category} successfully`, courses));
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Get courses by category failed.", {}, [{
        error_code: "GET_COURSES_BY_CATEGORY_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get a course in random
// @route   GET api/course/random
// @access  Public
exports.getARandomCourse = async (req, res) => {
  console.log("Getting a random course...");

  Course.aggregate([{ $sample: { size: 1 } }])
    .then((course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      course = course[0];

      // Get the instructor of the course.
      User.findById(course.instructor)
        .then((instructor) => {
          const instructorName = instructor ? instructor.username : "THIS_ACCOUNT_HAS_BEEN_DELETED";

          return res.status(200).json(formatResponse(req, true, "Get random course successfully", {
            _id: course._id,
            title: course.title,
            instructor: instructorName,
            description: course.description,
            price: course.price,
            thumbnail: course.thumbnail,
            category: course.category,
            students_count: course.students.length,
          }));
        })
        .catch((err) => {
          console.log(err);

          return res.status(400).json(formatResponse(req, false, "Get random course failed", {}, [{
            error_code: "GET_RANDOM_COURSE_FAILED", error_message: err.message,
          }]));
        });
    })
    .catch((err) => {
      console.log(err);

      return res.status(400).json(formatResponse(req, false, "Get random course failed", {}, [{
        error_code: "GET_RANDOM_COURSE_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Add course teaching assistant
// @route   PUT api/course/:_id/teaching-assistant
// @access  Private/Intructor
exports.addCourseTA = async (req, res) => {
  const { _id } = req.params;

  Course.findById(_id)
    .then(async (course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Check if the user is the course instructor
      if (course.instructor.toString() !== req.user._id.toString()) {
        return res.status(401).json(formatResponse(req, false, "Cannot add TA. You are not the course instructor.", {}, [{
          error_code: "NOT_INSTRUCTOR", error_message: "Cannot add TA. You are not the course instructor.",
        }]));
      }

      // Check if the user is existing
      User.findOne({ _id: req.body.ta_id })
        .then((ta) => {
          console.log(ta);
          if (!ta) {
            return res.status(404).json(formatResponse(req, false, "Add TA failed. User not found.", {}, [{
              error_code: "USER_NOT_FOUND", error_message: "Add TA failed. User not found.",
            }]));
          }

          // Check if the user is already a TA
          if (course.teaching_assistants.includes(ta._id)) {
            return res.status(400).json(formatResponse(req, false, "Cannot add TA. The user is already a TA of this course.", {}, [{
              error_code: "ALREADY_TA", error_message: "Cannot add TA. The user is already a TA of this course.",
            }]));
          }

          // Add the TA to the course
          course.teaching_assistants.unshift(ta._id);

          // Save the course to the database
          course
            .save()
            .then((course) => {
              return res.status(200).json(formatResponse(req, true, `TA ${ta.username} added successfully`, {
                  course_id: course._id,
                  course_title: course.title,
                  ta_id: ta._id,
                  ta_username: ta.username,
                }),
              );
            })
            .catch((err) => {
              return res.status(400).json(formatResponse(req, false, "Add TA failed.", {}, [{
                error_code: "ADD_TA_FAILED", error_message: err.message,
              }]));
            });
        })
        .catch((err) => {
          return res.status(400).json(formatResponse(req, false, "Add TA failed.", {}, [{
            error_code: "ADD_TA_FAILED", error_message: err.message,
          }]));
        });
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Add TA failed.", {}, [{
        error_code: "ADD_TA_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Remove course teaching assistant
// @route   DELETE api/course/:_id/teaching-assistant
// @access  Private/Intructor
exports.removeCourseTA = async (req, res) => {
  const { _id } = req.params;

  Course.findById(_id)
    .then((course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Check if the user is the course instructor
      if (course.instructor.toString() !== req.user._id.toString()) {
        return res.status(401).json(formatResponse(req, false, "Cannot remove TA. You are not the course instructor.", {}, [{
          error_code: "NOT_INSTRUCTOR", error_message: "Cannot remove TA. You are not the course instructor.",
        }]));
      }

      // Check if the user is already a TA
      if (!course.teaching_assistants.includes(req.body.ta_id)) {
        return res.status(400).json(formatResponse(req, false, "Cannot remove TA. The user is not a TA of this course.", {}, [{
          error_code: "NOT_TA", error_message: "Cannot remove TA. The user is not a TA of this course.",
        }]));
      }

      // Remove the TA from the course
      const index = course.teaching_assistants.indexOf(req.body.ta_id);
      const removedTA = course.teaching_assistants[index];
      course.teaching_assistants.splice(index, 1);

      // Save the course to the database
      course
        .save()
        .then((course) => {
          return res.status(200).json(formatResponse(req, true, `TA ${removedTA.username} removed successfully`, {
            course_id: course._id,
            course_title: course.title,
            ta_id: removedTA,
          }));
        })
        .catch((err) => {
          return res.status(400).json(formatResponse(req, false, "Remove TA failed.", {}, [{
            error_code: "REMOVE_TA_FAILED", error_message: err.message,
          }]));
        });
    })
    .catch((err) => {
      return res.status(400).json(formatResponse(req, false, "Remove TA failed.", {}, [{
        error_code: "REMOVE_TA_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Get course teaching assistants
// @route   GET api/course/:_id/teaching-assistant
// @access  Public
exports.getCourseTAs = async (req, res) => {
  const { _id } = req.params;

  Course.findById(_id)
    .then(async (course) => {
      if (!course) {
        return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
          error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
        }]));
      }

      // Return if there is no TA
      if (course.teaching_assistants.length === 0) {
        return res.status(200).json(formatResponse(req, true, "No TA found in this course", {}));
      }

      // Return the TA's username after getting all the TAs' usernames
      let TAs = [];
      for (let i = 0; i < course.teaching_assistants.length; i++) {
        try {
          const ta = await User.findById(course.teaching_assistants[i]);
          TAs.push(ta.username);
        } catch (err) {
          console.log(err);
          return res.status(400).json(formatResponse(req, false, "Get TA failed.", {}, [{
            error_code: "GET_TA_FAILED", error_message: err.message,
          }]));
        }
      }

      return res.status(200).json(formatResponse(req, true, `Found ${TAs.length} TAs`, TAs));
    })
    .catch((err) => {
      console.log(err);
      return res.status(400).json(formatResponse(req, false, "Get TA failed.", {}, [{
        error_code: "GET_TA_FAILED", error_message: err.message,
      }]));
    });
};

// @desc    Add student to course
// @route   POST api/course/student
// @access  Private
exports.addStudentToCourse = async (req, res) => {
  console.log("Adding student to course...");
  console.log(req.body);

  const { user_id, course_id } = req.body;

  Course.findById(course_id).then(async (course) => {
    if (!course) {
      return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
        error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
      }]));
    }

    // Check if the user is existed in the database
    const user = await User.findById(user_id);
    if (!user) {
      return res.status(404).json(formatResponse(req, false, "Add student failed. The user is not exist", {}, [{
        error_code: "USER_NOT_FOUND", error_message: "Add student failed. The user is not exist",
      }]));
    }

    // Check if the user is already enrolled
    if (course.students.includes(user_id)) {
      return res.status(400).json(formatResponse(req, false, "Cannot add this student to the course. This student is already enrolled in this course", {}, [{
        error_code: "ALREADY_ENROLLED",
        error_message: "Cannot add this student to the course. This student is already enrolled in this course",
      }]));
    }

    // Add the student to the course
    course.students.unshift(user_id);
    course.students_count = course.students.length;

    // Save the course to the database
    course
      .save()
      .then((course) => {
        return res.status(200).json(formatResponse(req, true, "Student added successfully", {
          course_id: course._id,
          course_title: course.title,
          student_id: user._id,
          student_username: user.username,
        }));
      })
      .catch((err) => {
        return res.status(400).json(
          formatResponse(req, false, "Course save failed, which lead to add student to course failed", {}, [{
            error_code: "COURSE_SAVE_FAILED", error_message: err.message,
          }]),
        );
      });
  });
};

// @desc    Remove student from course
// @route   DELETE api/course/student
// @access  Private
exports.removeStudentFromCourse = async (req, res) => {
  const { user_id, course_id } = req.body;

  Course.findById(course_id).then(async (course) => {
    if (!course) {
      return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
        error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
      }]));
    }

    // Check if the user is existed in the database
    const user = await User.findById(user_id);
    if (!user) {
      return res.status(404).json(formatResponse(req, false, "Remove student failed. The user is not exist", {}, [{
        error_code: "USER_NOT_FOUND", error_message: "Remove student failed. The user is not exist",
      }]));
    }
    // Check if the user is enrolled
    if (!course.students.includes(user_id)) {
      return res.status(400).json(formatResponse(req, false, "Cannot remove this student from the course. This student is not enrolled in this course", {}, [{
        error_code: "NOT_ENROLLED",
        error_message: "Cannot remove this student from the course. This student is not enrolled in this course",
      }]));
    }

    // Remove the student from the course
    const index = course.students.indexOf(user_id);
    course.students.splice(index, 1);

    // Save the course to the database
    course
      .save()
      .then((course) => {
        return res.status(200).json(formatResponse(req, true, "Student removed successfully", {
          course_id: course._id,
          course_title: course.title,
          student_id: user._id,
          student_username: user.username,
        }));
      })
      .catch((err) => {
        return res.status(400).json(
          formatResponse(req, false, "Course save failed, which lead to remove student from course failed", {}, [{
            error_code: "COURSE_SAVE_FAILED", error_message: err.message,
          }]),
        );
      });
  });
};

// @desc    Get course students
// @route   GET api/course/:_id/student
// @access  Public
exports.getCourseStudents = async (req, res) => {
  const { _id } = req.params;

  Course.findById(_id).then((course) => {
    if (!course) {
      return res.status(404).json(formatResponse(req, false, "Course not found.", {}, [{
        error_code: "COURSE_NOT_FOUND", error_message: "Course not found!",
      }]));
    }

    // Return if there is no student
    if (course.students.length === 0) {
      return res.status(200).json(formatResponse(req, true, "No student found in this course", {}));
    }

    // Get all the students' username
    let students = [];
    course.students.forEach((student_id) => {
      User.findById(student_id)
        .then(async (user) => {
          students.push(user.username);

          return students;
        })
        .then((students) => {
          console.log("students: ", students);
          // Return the students' username
          return res.status(200).json(formatResponse(req, true, `Found ${students.length} students`, students));
        })
        .catch((err) => {
          return res.status(400).json(formatResponse(req, false, "Get students failed.", {}, [{
            error_code: "GET_STUDENTS_FAILED", error_message: err.message,
          }]));
        });
    });
  });
};