const User = require("../../models/user-model");
const jwt = require("jsonwebtoken");
const loginValidation = require("../../validations/validation.js").loginValidation;
const formatResponse = require("../../utils/format-response.js");

// @desc    Login user
// @route   POST api/auth/login
// @access  Public
const login = (req, res) => {
  try {
    // Check if email and password are valid
    const { email, password } = req.body;
    const { error } = loginValidation({ email, password });
    if (error) return res.status(400).json(formatResponse(req, false, "Validation failed.", {}, [{
      error_code: "VALIDATION_ERROR", error_message: error.details[0].message,
    }]));

    // Find user and compare password
    User.findOne({ email: email }, function(err, user) {
      // Check if user exists
      if (err || !user) return res.status(401).json(formatResponse(req, false, err || "User not found!", {}, [{
        error_code: "USER_NOT_FOUND", error_message: "User not found!",
      }]));

      // Compare password
      user.comparePassword(password, function(err, isMatch) {
        if (err || !isMatch) return res.status(401).json(formatResponse(req, false, err || "Password is incorrect!", {}, [{
          error_code: "PASSWORD_INCORRECT", error_message: "Password is incorrect!",
        }]));

        // Create a token and assign it to user with 7 days for expiration time
        const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
          expiresIn: "7d",
        });

        return res.json(formatResponse(req, true, "Login successfully.", {
          token: token, user: {
            _id: user._id, username: user.username, email: user.email,
          },
        }));
      });
    }).select(["_id", "username", "email", "+password"]);
  } catch (err) {
    return res.status(500).json(formatResponse(req, false, "Could not sign in.", {}, [{
      error_code: "INTERNAL_SERVER_ERROR", error_message: err.message,
    }]));
  }
};

// @desc    Logout user
// @route   GET api/auth/logout
// @access  Private
const logout = (req, res) => {
  return res.status(200).json(formatResponse(req, true, "Logout successfully."));
};

module.exports = {
  login, logout,
};
