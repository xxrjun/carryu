// for api version >= 2
const formatResponse = (req, success, message, data = {}, errors = []) => {
  return {
    success: success,
    message: message,
    instance: `${req.originalUrl}`, // 使用req.originalUrl獲取當前URL路徑
    data: data,
    timestamp: new Date().toISOString(),
    errors: errors,
  };
};

module.exports = formatResponse;
