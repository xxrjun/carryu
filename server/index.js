const express = require("express");
require("dotenv").config();
const connectDB = require("./config/database.js");
const cors = require("cors");
const path = require("path");
const app = express();

app.use(cors());
app.set("trust proxy", true);
const PORT = process.env.PORT || 3001;

// Import routes
const apiRoute = require("./routes");

// Connect to database
connectDB()
  .then(() => {
    console.log("Connected to database");
  })
  .catch((err) => {
    console.log("Cannot connect to database", err);
    process.exit();
  });

// Init middleware
app.use(express.json()); // automatically parse incoming JSON to object
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, "build")));

// Routes
app.use("/api", apiRoute);

// Health check endpoint
app.get("/health", (req, res) => {
  return res.status(200).send("OK");
});

app.get("*", (req, res) => {
  console.log(path.join(__dirname, "build", "index.html"));

  res.sendFile(path.join(__dirname, "build", "index.html"));
});

// Listen port

const server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

module.exports = server;
