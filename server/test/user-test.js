const supertest = require("supertest");
const mongoose = require("mongoose");
const User = require("../models/user-model");
const sinon = require("sinon");
const { expect } = require("chai");

require("dotenv").config();
process.env.NODE_ENV = "test";

describe("User", function() {
  let server;
  let oldMongoURI = "";
  let oldPort = 8080;

  beforeEach("Re-require the server before each test", function() {
    // Stub console.log to suppress console output for successful tests
    consoleOutput = "";
    sinon.stub(console, "log").callsFake((msg) => {
      consoleOutput += msg + "\n";
    });
    // Clear cache to re-require server before each test
    delete require.cache[require.resolve("../index.js")];
    server = require("../index.js");
  });
  afterEach("Close the server after each test", function(done) {
    server.close(done);
    sinon.restore();
    if (this.currentTest.state === "failed") {
      console.log(consoleOutput);
    }
  });

  before("Drop old test database", function(done) {
    // Note: This method will break when extra query is provided in the connection string
    oldMongoURI = process.env.MONGO_URI;
    oldPort = process.env.PORT;
    process.env.MONGO_URI += "-test";
    process.env.PORT = 8082;
    mongoose.connect(process.env.MONGO_URI)
      .then(() => {
        // Sanity check
        expect(mongoose.connection.readyState).to.equal(1);
        expect(mongoose.connection.db.databaseName).to.not.equal("carryu");
        console.log(`Connected to database ${mongoose.connection.db.databaseName}`)
        mongoose.connection.db.dropDatabase(function() {
          done();
        });
      })
      .catch((err) => console.log(err));
  });
  after(function(done) {
    mongoose.connection.close(done);
    process.env.MONGO_URI = oldMongoURI;
    process.env.PORT = oldPort;
  });

  // describe("POST /api/user/register", function() {
  //   it("returns status 200 when the request body is provided correctly", function(done) {
  //     supertest(server)
  //       .post("/api/user/register")
  //       .send({
  //         "username": "test", "email": "test1@mail.com", "password": "12345678",
  //       })
  //       .expect(200, done);
  //   });
  //   it("returns status 400 with error message when username is empty", function(done) {
  //     supertest(server)
  //       .post("/api/user/register")
  //       .send({
  //         "username": "", "email": "test2@mail.com", "password": "12345678",
  //       })
  //       .expect(400, {
  //         err_msg: "\"username\" is not allowed to be empty",
  //       }, done);
  //   });
  //   it("returns status 400 with error message when username is too short", function(done) {
  //     supertest(server)
  //       .post("/api/user/register")
  //       .send({
  //         "username": "hi", "email": "test3@mail.com", "password": "12345678",
  //       })
  //       .expect(400, {
  //         err_msg: "\"username\" length must be at least 3 characters long",
  //       }, done);
  //   });
  //   it("returns status 400 with error message when email is empty", function(done) {
  //     supertest(server)
  //       .post("/api/user/register")
  //       .send({
  //         "username": "test", "email": "", "password": "12345678",
  //       })
  //       .expect(400, {
  //         err_msg: "\"email\" is not allowed to be empty",
  //       }, done);
  //   });
  //   it("returns status 400 with error message when an invalid email is provided", function(done) {
  //     supertest(server)
  //       .post("/api/user/register")
  //       .send({
  //         "username": "test", "email": "test5mail.com", "password": "12345678",
  //       })
  //       .expect(400, {
  //         err_msg: "\"email\" must be a valid email",
  //       }, done);
  //   });
  //   it("returns status 400 with error message when the email has already been registered", function() {
  //     const newUser = new User({
  //       username: "test_user", email: "test6@mail.com", password: "12345678",
  //     });
  //     expect(mongoose.connection.readyState).to.equal(1);
  //     return newUser.save().then(() => {
  //       return supertest(server)
  //         .post("/api/user/register")
  //         .send({
  //           "username": "test", "email": "test6@mail.com", "password": "12345678",
  //         })
  //         .expect(400, {
  //           err_msg: "Email has already been registered.",
  //         });
  //     });
  //   });
  //   it("returns status 400 with error message when password is empty", function(done) {
  //     supertest(server)
  //       .post("/api/user/register")
  //       .send({
  //         "username": "test", "email": "test7@mail.com", "password": "",
  //       })
  //       .expect(400, {
  //         err_msg: "\"password\" is not allowed to be empty",
  //       }, done);
  //   });
  //   it("returns status 400 with error message when password is too short", function(done) {
  //     supertest(server)
  //       .post("/api/user/register")
  //       .send({
  //         "username": "test", "email": "test8@mail.com", "password": "12345",
  //       })
  //       .expect(400, {
  //         err_msg: "\"password\" length must be at least 6 characters long",
  //       }, done);
  //   });
  // });
  describe("POST /api/v2/user/register", function() {
    const expectValidationError = (res) => {
      expect(res.body).to.have.property("success", false);
      expect(res.body).to.have.property("message", "Validation failed.");
      expect(res.body).to.have.property("instance", "/api/v2/user/register");
      expect(res.body).to.have.property("errors");
      expect(res.body.errors).to.have.lengthOf(1);
      expect(res.body.errors[0]).to.have.property("error_code", "VALIDATION_ERROR");
    };

    it("returns status 200 when the request body is provided correctly", function(done) {
      supertest(server)
        .post("/api/v2/user/register")
        .send({
          "username": "test", "email": "v2test1@mail.com", "password": "12345678",
        })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);
          expect(res.body).to.have.property("data");
          expect(res.body.data).to.have.property("username", "test");
          expect(res.body.data).to.have.property("email", "v2test1@mail.com");
          expect(res.body.data).to.have.property("role", "student");
          // Return hash instead of plain text password
          expect(res.body.data).to.have.property("password");
          expect(res.body.data).to.have.property("_id");
          return done();
        });
    });
    it("returns status 400 with error message when username is empty", function(done) {
      supertest(server)
        .post("/api/v2/user/register")
        .send({
          "username": "", "email": "v2test2@mail.com", "password": "12345678",
        })
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          expectValidationError(res);
          expect(res.body.errors[0]).to.have.property("error_message", "\"username\" is not allowed to be empty");
          return done();
        });
    });
    it("returns status 400 with error message when username is too short", function(done) {
      supertest(server)
        .post("/api/v2/user/register")
        .send({
          "username": "hi", "email": "v2test3@mail.com", "password": "12345678",
        })
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          expectValidationError(res);
          expect(res.body.errors[0]).to.have.property("error_message", "\"username\" length must be at least 3 characters long");
          return done();
        });
    });
    it("returns status 400 with error message when email is empty", function(done) {
      supertest(server)
        .post("/api/v2/user/register")
        .send({
          "username": "test", "email": "", "password": "12345678",
        })
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          expectValidationError(res);
          expect(res.body.errors[0]).to.have.property("error_message", "\"email\" is not allowed to be empty");
          return done();
        });
    });
    it("returns status 400 with error message when an invalid email is provided", function(done) {
      supertest(server)
        .post("/api/v2/user/register")
        .send({
          "username": "test", "email": "v2test5mail.com", "password": "12345678",
        })
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          expectValidationError(res);
          expect(res.body.errors[0]).to.have.property("error_message", "\"email\" must be a valid email");
          return done();
        });
    });
    it("returns status 400 with error message when the email has already been registered", function() {
      const email = "v2test6@mail.com";
      const newUser = new User({
        username: "test_user", email, password: "12345678",
      });
      expect(mongoose.connection.readyState).to.equal(1);
      return newUser.save().then(() => {
        return supertest(server)
          .post("/api/v2/user/register")
          .send({
            username: "test", email, password: "12345678",
          })
          .expect(400)
          .then((res) => {
            expect(res.body).to.have.property("success", false);
            expect(res.body).to.have.property("message", "Email has already been registered.");
            expect(res.body).to.have.property("instance", "/api/v2/user/register");
            expect(res.body).to.have.property("errors");
            expect(res.body.errors).to.have.lengthOf(1);
            expect(res.body.errors[0]).to.have.property("error_code", "EMAIL_ALREADY_REGISTERED");
            expect(res.body.errors[0]).to.have.property("error_message", "Email has already been registered.");
          });
      });
    });
    it("returns status 400 with error message when password is empty", function(done) {
      supertest(server)
        .post("/api/v2/user/register")
        .send({
          username: "test", email: "v2test7@mail.com", password: "",
        })
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          expectValidationError(res);
          expect(res.body.errors[0]).to.have.property("error_message", "\"password\" is not allowed to be empty");
          return done();
        });
    });
    it("returns status 400 with error message when password is too short", function(done) {
      supertest(server)
        .post("/api/v2/user/register")
        .send({
          username: "test", email: "v2test8@mail.com", password: "12345",
        })
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          expectValidationError(res);
          expect(res.body.errors[0]).to.have.property("error_message", "\"password\" length must be at least 6 characters long");
          return done();
        });
    });
  });
  describe("POST /api/v2/user/profile", function () {
    it("test_user_profile_update", async function() {
      const newUser = new User({
        username: "test_user", email: "v2test12@mail.com", password: "12345678",
      });
      await newUser.save();
      const res1 = await supertest(server)
        .post('/api/v2/auth/login')
        .send({email: "v2test12@mail.com", password: "12345678"})
        .expect(200);
      const token = res1.body.data.token;
      const res2 = await supertest(server)
        .patch('/api/v2/user/profile')
        .set("Authorization", `Bearer ${token}`)
        .send({username: "test_user12", email: "v2test1212@mail.com"})
        .expect(200);
      expect(res2.body).to.have.property("success", true);
      expect(res2.body).to.have.property("message", "Update user profile successfully");
      expect(res2.body.data).to.have.property("username", "test_user12");
      expect(res2.body.data).to.have.property("email", "v2test1212@mail.com");
  });
  })
});
