const supertest = require("supertest");
const mongoose = require("mongoose");
const User = require("../models/user-model");
const sinon = require("sinon");
const expect = require("chai").expect;
const assert = require('assert');

require("dotenv").config();
process.env.NODE_ENV = "test";

describe("Course", function () {
  let server;
  let oldMongoURI = "";
  beforeEach(function () {
    // Stub console.log to suppress console output for successful tests
    consoleOutput = "";
    sinon.stub(console, "log").callsFake((msg) => {
      consoleOutput += msg + "\n";
    });
    // Clear cache to re-require server before each test
    delete require.cache[require.resolve("../index.js")];
    server = require("../index.js");
  });
  afterEach(function (done) {
    server.close(done);
    sinon.restore();
    if (this.currentTest.state === "failed") {
      console.log(consoleOutput);
    }
  });

  before(function (done) {
    // Could be more flexible by using env to specify database name
    oldMongoURI = process.env.MONGO_URI;
    process.env.MONGO_URI += "-test";
    process.env.PORT = 8082;
    mongoose.connect(process.env.MONGO_URI)
      .then(() => {
        mongoose.connection.db.dropDatabase(function () {
          done();
        });
      })
      .catch((err) => console.log(err));
  });
  after(function (done) {
    mongoose.connection.close(done);
    process.env.MONGO_URI = oldMongoURI;
  });
  describe("POST /api/v2/course/create", function () {
    it("test_create_course", async function() {
      const newUser = new User({
        username: "test_user", email: "v2test13@mail.com", password: "12345678", role: "instructor",
      });
      await newUser.save();
      const res1 = await supertest(server)
        .post('/api/v2/auth/login')
        .send({email: "v2test13@mail.com", password: "12345678"})
        .expect(200);
      const token = res1.body.data.token;
      const userId = res1.body.data.user._id;
      const res2 = await supertest(server)
        .post('/api/v2/course/create')
        .set("Authorization", `Bearer ${token}`)
        .send({title: "test course", description: "test course", category: "finance", price: "1000",})
        .expect(200);
      expect(res2.body).to.have.property("success", true);
      expect(res2.body).to.have.property("message", "Course created successfully");
      expect(res2.body.data).to.have.property("instructor", userId);
      expect(res2.body.data).to.have.property("title", "test course");
    });

    it("test_create_course_with_short_title", async function() {
      const newUser = new User({
        username: "test_user", email: "v2test14@mail.com", password: "12345678", role: "instructor",
      });
      await newUser.save();
      const res1 = await supertest(server)
        .post('/api/v2/auth/login')
        .send({email: "v2test14@mail.com", password: "12345678"})
        .expect(200);
      const token = res1.body.data.token;
      const res2 = await supertest(server)
        .post('/api/v2/course/create')
        .set("Authorization", `Bearer ${token}`)
        .send({title: "14", description: "test course", category: "test category", price: "1000",})
        .expect(400);
      expect(res2.body).to.have.property("success", false);
      expect(res2.body).to.have.property("message", "Validation failed.");
      expect(res2.body.errors[0]).to.have.property("error_message", "\"title\" length must be at least 3 characters long");
    });

    it("test create course with empty price when user has logged in with instructor", function (done) {
      const testUser = new User({
        username: "test_intructor", email: "test15@mail.com", password: "12345678", role: "instructor",
      });
      expect(mongoose.connection.readyState).to.equal(1);
      testUser.save((err) => {
        if (err) return done(err);

        // Log in with the test user
        supertest(server)
          .post('/api/v2/auth/login')
          .send({ email: "test15@mail.com", password: "12345678" })
          .end((err, res) => {
            if (err) return done(err);
            // Send a request to the authenticated route    
            assert.equal(res.status, 200);
            assert.equal(res.body.message, "Login successfully.");
            const token = res.body.data.token;
            supertest(server)
              .post("/api/v2/course/create")
              .set('Authorization', `Bearer ${token}`)
              .send({
                "title": "course15", "description": "course15 description", "category": "finance", "price": "",
              })
              .end((err, res) => {
                if (err) return done(err);
                //assert.equal(res.status, 400);
                assert.equal(res.body.errors[0]["error_message"], "\"price\" must be a number");
                done();
              });

          });
      });

    });

    it("test create course with not authenticated user", async function () {
      expect(mongoose.connection.readyState).to.equal(1);
      const res1 = await supertest(server)
        .post("/api/v2/course/create")
        .send({
          title: "course16",
          description: "course16 description",
          category: "finance",
          price: 1000,
        })
        .expect(401);

      expect(res1.body).to.have.property("message", "Not authorized, no token available. Please login first.")

    });
  });
  describe("POST /api/v2/course/student", function () {
    it("test add student to course", async function () {
      const instructorTestUser = new User({
        username: "test_intructor17", email: "testInstructor17@mail.com", password: "12345678", role: "instructor"
      });
      const studentTestUser = new User({
        username: "test_student17", email: "testStudent17@mail.com", password: "12345678"
      });
      expect(mongoose.connection.readyState).to.equal(1);
      await instructorTestUser.save();
      await studentTestUser.save();
      const instructorAccountRes = await supertest(server)
        .post('/api/v2/auth/login')
        .send({
          email: "testInstructor17@mail.com",
          password: "12345678"
        })
        .expect(200)
      const instructorToken = instructorAccountRes.body.data.token;

      const instructorCourseRes = await supertest(server)
        .post('/api/v2/course/create')
        .set('Authorization', `Bearer ${instructorToken}`)
        .send({
          title: "course17",
          description: "course17 description",
          category: "finance",
          price: 1000,
        })
        .expect(200);


      const studentAccountRes = await supertest(server)
        .post('/api/v2/auth/login')
        .send({
          email: "testStudent17@mail.com",
          password: "12345678"
        })
        .expect(200)
      const studentToken = studentAccountRes.body.data.token;
      const studentAddCourseRes = await supertest(server)
        .post('/api/v2/course/student')
        .set('Authorization', `Bearer ${studentToken}`)
        .send({
          user_id: studentAccountRes.body.data.user._id,
          course_id: instructorCourseRes.body.data._id
        })
        .expect(200);
    });

    it("test add student to course with enrolled student", async function () {
      const instructorTestUser = new User({
        username: "test_intructor18", email: "testInstructor18@mail.com", password: "12345678", role: "instructor"
      });
      const studentTestUser = new User({
        username: "test_student18", email: "testStudent18@mail.com", password: "12345678"
      });
      expect(mongoose.connection.readyState).to.equal(1);
      await instructorTestUser.save();
      await studentTestUser.save();
      const instructorAccountRes = await supertest(server)
        .post('/api/v2/auth/login')
        .send({
          email: "testInstructor18@mail.com",
          password: "12345678"
        })
        .expect(200)
      const instructorToken = instructorAccountRes.body.data.token;

      const instructorCourseRes = await supertest(server)
        .post('/api/v2/course/create')
        .set('Authorization', `Bearer ${instructorToken}`)
        .send({
          title: "course18",
          description: "course18 description",
          category: "finance",
          price: 1000,
        })
        .expect(200);


      const studentAccountRes = await supertest(server)
        .post('/api/v2/auth/login')
        .send({
          email: "testStudent18@mail.com",
          password: "12345678"
        })
        .expect(200)
      const studentToken = studentAccountRes.body.data.token;

      const studentAddCourseRes1 = await supertest(server)
        .post('/api/v2/course/student')
        .set('Authorization', `Bearer ${studentToken}`)
        .send({
          user_id: studentAccountRes.body.data.user._id,
          course_id: instructorCourseRes.body.data._id
        })
        .expect(200);

      const studentAddCourseRes2 = await supertest(server)
        .post('/api/v2/course/student')
        .set('Authorization', `Bearer ${studentToken}`)
        .send({
          user_id: studentAccountRes.body.data.user._id,
          course_id: instructorCourseRes.body.data._id
        })
        .expect(400);
      expect(studentAddCourseRes2.body).to.have.property("message", "Cannot add this student to the course. This student is already enrolled in this course")
    });
  });

  describe("POST /api/v2/course/:_id/comment", function () {
    it("test add course comment", async function () {
      const instructorTestUser = new User({
        username: "test_intructor19", email: "testInstructor19@mail.com", password: "12345678", role: "instructor"
      });
      const studentTestUser = new User({
        username: "test_student19", email: "testStudent19@mail.com", password: "12345678"
      });
      expect(mongoose.connection.readyState).to.equal(1);
      await instructorTestUser.save();
      await studentTestUser.save();
      const instructorAccountRes = await supertest(server)
        .post('/api/v2/auth/login')
        .send({
          email: "testInstructor19@mail.com",
          password: "12345678"
        })
        .expect(200)
      const instructorToken = instructorAccountRes.body.data.token;

      const instructorCourseRes = await supertest(server)
        .post('/api/v2/course/create')
        .set('Authorization', `Bearer ${instructorToken}`)
        .send({
          title: "course18",
          description: "course19 description",
          category: "finance",
          price: 1000,
        })
        .expect(200);


      const studentAccountRes = await supertest(server)
        .post('/api/v2/auth/login')
        .send({
          email: "testStudent19@mail.com",
          password: "12345678"
        })
        .expect(200)
      const studentToken = studentAccountRes.body.data.token;

      const studentAddCourseRes1 = await supertest(server)
        .post('/api/v2/course/student')
        .set('Authorization', `Bearer ${studentToken}`)
        .send({
          user_id: studentAccountRes.body.data.user._id,
          course_id: instructorCourseRes.body.data._id
        })
        .expect(200);

      const studentAddcommentRes = await supertest(server)
        .post('/api/v2/course/' + instructorCourseRes.body.data._id + '/comment')
        .set('Authorization', `Bearer ${studentToken}`)
        .send({
          comment: "course comment"
        })
        .expect(200)
    });
  });

});
