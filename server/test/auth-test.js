const supertest = require("supertest");
const mongoose = require("mongoose");
const User = require("../models/user-model");
const sinon = require("sinon");
const expect = require("chai").expect;
const assert = require('assert');

require("dotenv").config();
process.env.NODE_ENV = "test";
mongoose.set("strictQuery", true);

describe("Auth", function () {
    let server;
    let oldMongoURI = "";
    beforeEach(function () {
        // Stub console.log to suppress console output for successful tests
        consoleOutput = "";
        sinon.stub(console, "log").callsFake((msg) => {
            consoleOutput += msg + "\n";
        });
        // Clear cache to re-require server before each test
        delete require.cache[require.resolve("../index.js")];
        server = require("../index.js");
    });
    afterEach(function (done) {
        server.close(done);
        sinon.restore();
        if (this.currentTest.state === "failed") {
            console.log(consoleOutput);
        }
    });

    before(function (done) {
        // Could be more flexible by using env to specify database name
        oldMongoURI = process.env.MONGO_URI;
        process.env.MONGO_URI += "-test";
        process.env.PORT = 8082;
        mongoose.connect(process.env.MONGO_URI)
            .then(() => {
                mongoose.connection.db.dropDatabase(function () {
                    done();
                });
            })
            .catch((err) => console.log(err));
    });
    after(function (done) {
        mongoose.connection.close(done);
        process.env.MONGO_URI = oldMongoURI;
    });
    describe("POST /api/v2/auth/login", function () {
        it("test_auth_login", async function() {
            const newUser = new User({
              username: "test_user", email: "v2test9@mail.com", password: "12345678",
            });
            await newUser.save();
            const res = await supertest(server)
              .post('/api/v2/auth/login')
              .send({email: "v2test9@mail.com", password: "12345678"})
              .expect(200);
            expect(res.body).to.have.property("success", true);
            expect(res.body).to.have.property("message", "Login successfully.");
        });

        it("test_auth_login_with_unregistered_email", function(done) {
            supertest(server) 
              .post("/api/v2/auth/login")
              .send({               
                  "email": "student010@fakemail.com",
                  "password": "password"
                })
              .end((err, res) => {
                  try {
                      if(err) throw err;
                      expect(res.body).to.have.property("success", false);
                      expect(res.body.errors[0]).to.have.property("error_message", "User not found!");
                      done();
                  } catch (error) {
                      done(error);
                  }
              });
        });

        it("test_auth_login_with_wrong_password", async function() {
            const newUser = new User({
              username: "test_user", email: "v2test9@mail.com", password: "12345678",
            });
            await newUser.save();
            const res = await supertest(server)
              .post('/api/v2/auth/login')
              .send({email: "v2test9@mail.com", password: "123456789"})
              .expect(401);
            expect(res.body).to.have.property("success", false);
            expect(res.body).to.have.property("message", "Password is incorrect!");
            expect(res.body.errors[0]).to.have.property("error_message", "Password is incorrect!");
        });
    })

    describe("POST /api/v2/auth/logout", function () {
        it("test auth logout", async function () {
            const testUser = new User({
                username: "test_user20", email: "testUser20@mail.com", password: "12345678"
            });
            expect(mongoose.connection.readyState).to.equal(1);
            await testUser.save();
            const userAccountRes = await supertest(server)
                .post('/api/v2/auth/login')
                .send({
                    email: "testUser20@mail.com",
                    password: "12345678"
                })
                .expect(200)

            const userToken = userAccountRes.body.data.token;
            const userLogoutRes = await supertest(server)
                .get('/api/v2/auth/logout')
                .set('Authorization', `Bearer ${userToken}`)
                .expect(200)
        });
    });


});
